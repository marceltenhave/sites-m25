<?php
/**
 * The Right Sidebar.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>
 <div id="Rechterkolom">
 <div id="DatumM25"> <?php echo date_i18n( get_option( 'date_format' ) ) ?></div>
      <div id="Teller">Bezoekersteller</p>
      <p>De teller is gestart op<br />
        31 oktober 2005 </p>
      <p>U bent bezoeker </p>


        </div><!-- End of Teller -->
        				<div class="widget-area" role="complementary">
					<?php if ( ! dynamic_sidebar( 'sidebar-6' ) ) : ?>

						<?php
						the_widget( 'Right Sidebar', '', array( 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
						?>

					<?php endif; // end sidebar widget area ?>
				</div><!-- .widget-area -->
</div> <!-- Einde van Rechterkolom -->
<?php endif; ?>