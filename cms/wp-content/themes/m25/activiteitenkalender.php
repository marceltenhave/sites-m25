<?php
/**
 * Template Name: Activiteiten Kalender
 *
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
  <div id="Activiteitenkalender">
  <div id="Centralekolom">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>
</div><!-- Einde van Centralekolom -->
</div><!-- Einde van Voorpagina -->

<?php get_footer(); ?>